import fr.epsi.netflix.Movie;
import fr.epsi.netflix.MovieFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MovieFactoryTest {
	@Test

	public void should_return_movie_when_string_array_of_size_11_is_provided(){
		MovieFactory mf = new MovieFactory();
		String[] items = new String[]{"s1","Movie","Dick Johnson Is Dead","Kirsten Johnson","TestCast","United States","September 25 2021","2020","PG-13","90 min","Documentaries","As her father nears the end of his life filmmaker Kirsten Johnson stages his death in inventive and comical ways to help them both face the inevitable."};
		Movie movie = mf.createMovie(items);

		Assertions.assertEquals("s1",movie.getId());
		Assertions.assertEquals("Movie",movie.getType());
		Assertions.assertEquals("Dick Johnson Is Dead",movie.getTitle());
		Assertions.assertEquals("Kirsten Johnson",movie.getDirector());
		Assertions.assertEquals("TestCast",movie.getCast());
		Assertions.assertEquals("United States",movie.getCountry());
		Assertions.assertEquals(2020,movie.getReleaseYear());
		Assertions.assertEquals("PG-13",movie.getRating());
		Assertions.assertEquals("90 min",movie.getDuration());
		Assertions.assertEquals("Documentaries",movie.getListedIn());
		Assertions.assertEquals("As her father nears the end of his life filmmaker Kirsten Johnson stages his death in inventive and comical ways to help them both face the inevitable.",movie.getDescription());
	}
}
